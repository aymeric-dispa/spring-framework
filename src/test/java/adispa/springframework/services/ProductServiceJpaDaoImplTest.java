package adispa.springframework.services;

import adispa.springframework.config.JpaIntegrationConfig;
import adispa.springframework.domain.Product;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(JpaIntegrationConfig.class)
@WebAppConfiguration
@ActiveProfiles("jpadao")
public class ProductServiceJpaDaoImplTest {

    private ProductService productService;

    @Autowired
    public void setProductService(ProductService productService) {
        this.productService = productService;
    }



    @Test
    public void testGetByIdMethod() throws Exception {
        Product product = (Product) productService.getById(1);
        assert product.getDescription().equals("Product 1");
    }

    @Test
    public void testSaveOrUpdateMethod() throws Exception {
        Integer listSize = ((List<Product>) productService.listAll()).size();
        String descr = "Random Description";
        int id = 2;
        Product product = (Product) productService.getById(id);
        product.setDescription(descr);
        productService.saveOrUpdate(product);


        //check that an update occurs
        assert productService.getById(id).getDescription().equals(descr);
        assert listSize.equals(productService.listAll().size());

        //check that an insert occurs

        Product prod = new Product();
        productService.saveOrUpdate(prod);
        assert !listSize.equals(productService.listAll().size());

    }

    @Test
    public void testDeleteMethod() throws Exception {
        Integer id = 3;
        Integer listSize = ((List<Product>) productService.listAll()).size();
        productService.delete(id);

        assert productService.listAll().size()==listSize-1;
        assert productService.getById(id) == null;

    }


}
