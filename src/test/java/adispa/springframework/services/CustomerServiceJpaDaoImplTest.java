package adispa.springframework.services;

import adispa.springframework.config.JpaIntegrationConfig;
import adispa.springframework.domain.Customer;
import adispa.springframework.domain.User;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(JpaIntegrationConfig.class)
@ActiveProfiles("jpadao")
@WebAppConfiguration
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CustomerServiceJpaDaoImplTest {

    private CustomerService customerService;

    @Autowired
    public void setCustomerService(CustomerService customerService) {
        this.customerService = customerService;
    }



    @Test
    public void testGetByIdMethod() throws Exception {
        Customer customer = (Customer) customerService.getById(1);
        assert customer.getEmail().equals("diane@gmail.com");
    }

    @Test
    public void testSaveOrUpdateMethod() throws Exception {
        Integer listSize = ((List<Customer>) customerService.listAll()).size();
        String firstName = "Georges";
        int id = 2;
        Customer customer = (Customer) customerService.getById(id);
        customer.setFirstName(firstName);
        customerService.saveOrUpdate(customer);


        //check that an update occurs
        assert customerService.getById(id).getFirstName().equals(firstName);
        assert listSize.equals(customerService.listAll().size());

        //check that an insert occurs

        Customer prod = new Customer();
        customerService.saveOrUpdate(prod);
        assert !listSize.equals(customerService.listAll().size());

    }

    @Test
    public void testSaveWithUser() {

        Customer customer = new Customer();
        User user = new User();
        user.setUsername("UserName");
        user.setPassword("Password");
        customer.setUser(user);

        Customer savedCustomer = customerService.saveOrUpdate(customer);

        assert savedCustomer.getUser().getId() != null;
    }

}
