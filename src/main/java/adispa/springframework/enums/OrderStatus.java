package adispa.springframework.enums;


public enum OrderStatus {
    NEW, ALLOCATED, SHIPPED
}
