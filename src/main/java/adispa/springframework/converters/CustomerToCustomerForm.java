package adispa.springframework.converters;

import adispa.springframework.commands.CustomerForm;
import adispa.springframework.domain.Address;
import adispa.springframework.domain.Customer;
import adispa.springframework.domain.User;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class CustomerToCustomerForm implements Converter<Customer, CustomerForm> {

    @Override
    public CustomerForm convert(Customer customer) {
        CustomerForm customerForm = new CustomerForm();
        customerForm.setUserId(customer.getUser().getId());
        customerForm.setUserVersion(customer.getUser().getVersion());
        customerForm.setCustomerId(customer.getId());
        customerForm.setCustomerVersion(customer.getVersion());
        customerForm.setUserName(customer.getUser().getUsername());
        customerForm.setPasswordText(customer.getUser().getPassword());
        customerForm.setPasswordTextConfirmation(customer.getUser().getPassword());
        customerForm.setFirstName(customer.getFirstName());
        customerForm.setLastName(customer.getLastName());
        customerForm.setEmail(customer.getEmail());
        customerForm.setPhoneNumber(customer.getPhoneNumber());
        return customerForm;
    }
}
