package adispa.springframework.repositories;

import adispa.springframework.domain.Customer;
import adispa.springframework.domain.security.Role;
import org.springframework.data.repository.CrudRepository;


public interface RoleRepository extends CrudRepository<Role, Integer> {
}
