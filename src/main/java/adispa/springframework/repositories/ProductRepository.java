package adispa.springframework.repositories;

import adispa.springframework.domain.Product;
import org.springframework.data.repository.CrudRepository;


public interface ProductRepository extends CrudRepository<Product, Integer> {
}
