package adispa.springframework.repositories;

import adispa.springframework.domain.User;
import adispa.springframework.domain.security.Role;
import org.springframework.data.repository.CrudRepository;


public interface UserRepository extends CrudRepository<User, Integer> {
    User findByUsername(String userName);
}
