package adispa.springframework.bootstrap;

import adispa.springframework.domain.*;
import adispa.springframework.domain.security.Role;
import adispa.springframework.services.ProductService;
import adispa.springframework.services.RoleService;
import adispa.springframework.services.UserService;
import adispa.springframework.enums.OrderStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;


@Component
public class SpringJpaBootstrap implements ApplicationListener<ContextRefreshedEvent> {

    private ProductService productService;
    private UserService userService;
    private RoleService roleService;
    @Autowired
    public void setProductService(ProductService productService) {
        this.productService = productService;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setRoleService(RoleService roleService) {
        this.roleService = roleService;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        loadProducts();
        loadUsersAndCustomers();
        loadCarts();
        loadOrderHistory();
        loadRoles();
        assignUsersToDefaultRole();
        assignUsersToAdminRole();
    }


    public void loadProducts() {
        Product product1 = new Product();
        product1.setDescription("Product 1");
        product1.setPrice(new BigDecimal("12.99"));
        product1.setImageUrl("http://example.com/product1");
        productService.saveOrUpdate(product1);

        Product product2 = new Product();
        product2.setDescription("Product 2");
        product2.setPrice(new BigDecimal("14.99"));
        product2.setImageUrl("http://example.com/product2");
        productService.saveOrUpdate(product2);

        Product product3 = new Product();
        product3.setDescription("Product 3");
        product3.setPrice(new BigDecimal("34.99"));
        product3.setImageUrl("http://example.com/product3");
        productService.saveOrUpdate(product3);

        Product product4 = new Product();
        product4.setDescription("Product 4");
        product4.setPrice(new BigDecimal("44.99"));
        product4.setImageUrl("http://example.com/product4");
        productService.saveOrUpdate(product4);

        Product product5 = new Product();
        product5.setDescription("Product 5");
        product5.setPrice(new BigDecimal("25.99"));
        product5.setImageUrl("http://example.com/product5");
        productService.saveOrUpdate(product5);
    }

    public void loadUsersAndCustomers() {
        User user1 = new User();
        user1.setUsername("dnguyen");
        user1.setPassword("password");

        Customer customer1 = new Customer();
        Address billingAddress1=new Address();
        Address shippingAddress1=billingAddress1;
        customer1.setLastName("Nguyen");
        customer1.setFirstName("Diane");
        customer1.setEmail("diane@gmail.com");
        customer1.setPhoneNumber("089526930");

        billingAddress1.setAddressLine1("12 Down Street");
        billingAddress1.setCity("Dublin");
        billingAddress1.setState("N/A");
        billingAddress1.setZipCode("D15");
        customer1.setShippingAddress(shippingAddress1);
        customer1.setBillingAddress(billingAddress1);
        user1.setCustomer(customer1);
        userService.saveOrUpdate(user1);

        User user2 = new User();
        user2.setUsername("dcabril");
        user2.setPassword("password");
        Customer customer2 = new Customer();
        Address billingAddress2=new Address();
        Address shippingAddress2=new Address();
        customer2.setLastName("Cabril");
        customer2.setFirstName("Danny");
        customer2.setEmail("danny@gmail.com");
        customer2.setPhoneNumber("089526931");
        billingAddress2.setAddressLine1("14 Up Street");
        billingAddress2.setCity("Dublin");
        billingAddress2.setState("N/A");
        billingAddress2.setZipCode("D12");
        shippingAddress2.setAddressLine1("19 Up Street");
        shippingAddress2.setCity("Dublin");
        shippingAddress2.setState("N/A");
        shippingAddress2.setZipCode("D4");
        customer2.setShippingAddress(shippingAddress2);
        customer2.setBillingAddress(billingAddress2);
        user2.setCustomer(customer2);
        userService.saveOrUpdate(user2);

        User user3 = new User();
        user3.setUsername("ddoe");
        user3.setPassword("password");
        Customer customer3 = new Customer();
        Address billingAddress3=new Address();
        Address shippingAddress3=billingAddress3;
        customer3.setLastName("Doe");
        customer3.setFirstName("John");
        customer3.setEmail("johnd@gmail.com");
        customer3.setPhoneNumber("089528734");
        billingAddress3.setAddressLine1("85 St Street");
        billingAddress3.setCity("Dublin");
        billingAddress3.setState("N/A");
        billingAddress3.setZipCode("D1");
        customer3.setShippingAddress(shippingAddress3);
        customer3.setBillingAddress(billingAddress3);
        user3.setCustomer(customer3);
        userService.saveOrUpdate(user3);

    }

    private void loadOrderHistory() {
        List<User> users = (List<User>) userService.listAll();
        List<Product> products = (List<Product>) productService.listAll();

        users.forEach(user ->{
            Order order = new Order();
            order.setCustomer(user.getCustomer());
            order.setOrderStatus(OrderStatus.SHIPPED);

            products.forEach(product -> {
                OrderDetail orderDetail = new OrderDetail();
                orderDetail.setProduct(product);
                orderDetail.setQuantity(1);
                order.addToOrderDetails(orderDetail);
            });
        });
    }

    private void loadCarts() {
        List<User> users = (List<User>) userService.listAll();
        List<Product> products = (List<Product>) productService.listAll();

        users.forEach(user -> {
            user.setCart(new Cart());
            CartDetail cartDetail = new CartDetail();
            cartDetail.setProduct(products.get(0));
            cartDetail.setQuantity(2);
            user.getCart().addCartDetail(cartDetail);
            userService.saveOrUpdate(user);
        });
    }

    private void loadRoles() {
        Role role = new Role();
        role.setRole("CUSTOMER");
        roleService.saveOrUpdate(role);

        Role adminRole = new Role();
        adminRole.setRole("ADMIN");
        roleService.saveOrUpdate(adminRole);
    }

    private void assignUsersToDefaultRole() {
        List<Role> roles = (List<Role>) roleService.listAll();
        List<User> users = (List<User>) userService.listAll();

        roles.forEach(role ->{
            if(role.getRole().equalsIgnoreCase("CUSTOMER")){
                users.forEach(user -> {
                    user.addRole(role);
                    userService.saveOrUpdate(user);
                });
            }
        });

    }

    private void assignUsersToAdminRole() {
        List<Role> roles = (List<Role>) roleService.listAll();
        List<User> users = (List<User>) userService.listAll();

        roles.forEach(role ->{
            if(role.getRole().equalsIgnoreCase("ADMIN")){
                users.forEach(user -> {
                    if(user.getUsername().equals("dnguyen")){
                        user.addRole(role);
                        userService.saveOrUpdate(user);
                    }
                });
            }
        });

    }


}
