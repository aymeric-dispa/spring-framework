package adispa.springframework.services.mapservices;

import adispa.springframework.commands.CustomerForm;
import adispa.springframework.converters.CustomerFormToCustomer;
import adispa.springframework.converters.CustomerToCustomerForm;
import adispa.springframework.domain.Customer;
import adispa.springframework.domain.DomainObject;
import adispa.springframework.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@Profile("map")
public class CustomerServiceImpl extends AbstractMapService implements CustomerService {

    private CustomerFormToCustomer customerFormToCustomer;


    @Autowired
    public void setCustomerFormToCustomer(CustomerFormToCustomer customerFormToCustomer) {
        this.customerFormToCustomer = customerFormToCustomer;
    }


    @Override
    public List<DomainObject> listAll() {
        return super.listAll();
    }

    @Override
    public Customer getById(Integer id) {
        return (Customer) super.getById(id);
    }

    @Override
    public Customer saveOrUpdate(Customer customer) {
        return (Customer) super.saveOrUpdate(customer);
    }

    @Override
    public void delete(Integer id) {
        super.delete(id);
    }


    @Override
    public Customer saveOrUpdateCustomerForm(CustomerForm customerForm) {
        Customer customer=customerFormToCustomer.convert(customerForm);
        if (customer.getUser().getId()!=null){
            Customer existingCustomer=getById(customer.getId());

            customer.getUser().setEnabled(existingCustomer.getUser().getEnabled());
        }
        return saveOrUpdate(customer);
    }


}
