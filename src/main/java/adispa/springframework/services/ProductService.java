package adispa.springframework.services;

import adispa.springframework.commands.ProductForm;
import adispa.springframework.domain.Product;


public interface ProductService extends CRUDService<Product> {
    Product saveOrUpdateProductForm(ProductForm productForm);
}