package adispa.springframework.services;


public interface SendTextMessageService {

    void sendTextMessage(String msg);

}
