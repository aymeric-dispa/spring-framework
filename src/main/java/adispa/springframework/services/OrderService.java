package adispa.springframework.services;

import adispa.springframework.domain.Order;
import adispa.springframework.domain.Product;


public interface OrderService extends CRUDService<Order> {

}