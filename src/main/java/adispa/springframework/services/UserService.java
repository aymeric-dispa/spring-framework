package adispa.springframework.services;

import adispa.springframework.domain.User;


public interface UserService extends CRUDService<User> {

    User findByUserName(String userName);

}