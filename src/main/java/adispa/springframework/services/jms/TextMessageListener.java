package adispa.springframework.services.jms;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class TextMessageListener {

    @JmsListener(destination ="text.messagequeue")//matches the queue name in JMSConfig class
    public void onMessage(String msg){
        System.out.println("######################");
        System.out.println("MESSAGE : ");
        System.out.println(msg);
        System.out.println("######################");
    }

}
