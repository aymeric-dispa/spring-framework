package adispa.springframework.services.reposervices;

import adispa.springframework.domain.security.Role;
import adispa.springframework.repositories.RoleRepository;
import adispa.springframework.services.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Profile("springdatajpa")
public class RoleServiceRepoImpl implements RoleService {
    private RoleRepository productRepository;

    @Autowired
    public void setRoleRepository(RoleRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<?> listAll() {
        List<Role> products = new ArrayList<>();
        productRepository.findAll().forEach(products::add);
        return products;
    }

    @Override
    public Role getById(Integer id) {
        return productRepository.findOne(id);
    }

    @Override
    public Role saveOrUpdate(Role domainObject) {
        return productRepository.save(domainObject);
    }

    @Override
    public void delete(Integer id) {
        productRepository.delete(id);
    }
}
