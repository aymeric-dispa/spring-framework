package adispa.springframework.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import javax.jms.Queue;

@Component
public class SendTextMessageServiceImpl implements SendTextMessageService {

    private Queue textMessQueue;
    private JmsTemplate jmsTemplate;

    @Autowired
    public void setTextMessQueue(Queue textMessQueue) {
        this.textMessQueue = textMessQueue;
    }

    @Autowired
    public void setJmsTemplate(org.springframework.jms.core.JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }

    @Override
    public void sendTextMessage(String msg) {
        this.jmsTemplate.convertAndSend(this.textMessQueue, msg);

    }
}
