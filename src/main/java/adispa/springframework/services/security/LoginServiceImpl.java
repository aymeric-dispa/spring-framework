package adispa.springframework.services.security;


import adispa.springframework.domain.User;
import adispa.springframework.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    private UserService userService;

    @Scheduled(fixedRate = 60000) //every 1 minute
    @Override
    public void resetFailedLogin() {
        System.out.println("Checking for locked account");
        List<User> userList = (List<User>) userService.listAll();
        userList.forEach(user -> {
            if (!user.getEnabled() && user.getFailedLoginAttempts() > 0) {
                System.out.println("Resetting failed attempts for user : " + user.getUsername());
                user.setFailedLoginAttempts(0);
                user.setEnabled(true);
                userService.saveOrUpdate(user);
            }
        });
    }
}
