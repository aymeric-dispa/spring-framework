package adispa.springframework.services.security;


public interface LoginService {

    void resetFailedLogin();
}
