package adispa.springframework.services;

import adispa.springframework.domain.User;
import adispa.springframework.domain.security.Role;


public interface RoleService extends CRUDService<Role> {

}