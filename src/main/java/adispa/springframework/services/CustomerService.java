package adispa.springframework.services;

import adispa.springframework.commands.CustomerForm;
import adispa.springframework.domain.Customer;

import java.util.List;


public interface CustomerService extends CRUDService<Customer>{

    Customer saveOrUpdateCustomerForm(CustomerForm customerForm);

}
